﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System.Windows.Controls.DataVisualization.Charting.Data
{
    public interface IVerticalModel : IComparable
    {
        double Min { get; set; }

        double Max { get; set; }
    }
}
