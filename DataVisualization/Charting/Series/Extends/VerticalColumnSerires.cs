﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.DataVisualization.Charting.Data;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace System.Windows.Controls.DataVisualization.Charting
{
    /// <summary>
    /// 垂直线条图，纵坐标需要提供一个最小值和最大值，纵坐标数据对象需要继承IVerticalModel
    /// </summary>
    public class VerticalColumnSerires : ColumnBarBaseSeries<ColumnDataPoint>
    {
        public string YAxisName { get; set; }


        private IRangeAxis _temp;

        /// <summary>
        /// 柱型宽度
        /// </summary>
        public double ColumnWidth { get; set; }

        /// <summary>
        /// VerticalColumnSerires
        /// </summary>
        public VerticalColumnSerires()
        {
            ColumnWidth = 10;
        }

        /// <summary>
        /// 这里替换了原有获取纵坐标轴的逻辑
        /// 原有逻辑在获取坐标轴时如果数据对象对应的数据不是double或者DataTime，会调用最后一个委托生成一个坐标轴
        /// </summary>
        /// <param name="firstDataPoint"></param>
        protected override void GetAxes(DataPoint firstDataPoint)
        {
            //这里替换了原有获取纵坐标轴的逻辑
            //原有逻辑在获取坐标轴时如果数据对象对应的数据不是double或者DataTime，会调用最后一个委托生成一个坐标轴
            this.GetAxes(firstDataPoint, axis => axis.Orientation == AxisOrientation.X, () => new CategoryAxis { Orientation = AxisOrientation.X }, delegate(IAxis axis)
            {
                if (YAxisName.Equals((axis as FrameworkElement).Name))
                {
                    this._temp = axis as IRangeAxis;        //待修改
                }

                if (string.IsNullOrEmpty(YAxisName))
                {
                    return axis.Orientation == AxisOrientation.Y && axis is IRangeAxis;
                }
                else
                {
                    return YAxisName.Equals((axis as FrameworkElement).Name);
                }
            }, delegate
            {
                //IRangeAxis axis = DataPointSeriesWithAxes.CreateRangeAxisFromData(100);
                //axis.Orientation = AxisOrientation.Y;
                //if ((axis == null) || (axis.Origin == null))
                //{
                //    throw new InvalidOperationException("---");
                //}
                //DisplayAxis axis2 = axis as DisplayAxis;
                //if (axis2 != null)
                //{
                //    axis2.ShowGridLines = true;
                //}
                //return axis;

                return this._temp;
            });
        }

        /// <summary>
        /// UpdateDataPoint
        /// </summary>
        /// <param name="dataPoint"></param>
        protected override void UpdateDataPoint(DataPoint dataPoint)
        {
            if (base.SeriesHost != null)
            //if ((base.SeriesHost != null) && (base.PlotArea != null))
            {
                object category = dataPoint.ActualIndependentValue ?? (this.ActiveDataPoints.IndexOf(dataPoint) + 1);
                Range<UnitValue> categoryRange = base.GetCategoryRange(category);
                if (categoryRange.HasData)
                {
                    if ((categoryRange.Maximum.Unit != Unit.Pixels) || (categoryRange.Minimum.Unit != Unit.Pixels))
                    {
                        throw new InvalidOperationException("---");
                    }
                    double num = categoryRange.Minimum.Value;
                    double num2 = categoryRange.Maximum.Value;
                    double num3 = base.ActualDependentRangeAxis.GetPlotAreaCoordinate(base.ActualDependentRangeAxis.Range.Maximum).Value;
                    IEnumerable<VerticalColumnSerires> source = from series in base.SeriesHost.Series.OfType<VerticalColumnSerires>()
                                                          where series.ActualIndependentAxis == base.ActualIndependentAxis
                                                          select series;
                    int num4 = source.Count<VerticalColumnSerires>();
                    double num5 = num2 - num;
                    double num6 = num5 * 0.8;
                    double a = num6 / ((double)num4);
                    int index = source.IndexOf(this);
                    //double num9 = base.ActualDependentRangeAxis.GetPlotAreaCoordinate(ValueHelper.ToDouble(dataPoint.ActualDependentValue)).Value;

                    double tempNum = ValueHelper.ToDouble((dataPoint.ActualDependentValue as IVerticalModel).Max);

                    double num9 = base.ActualDependentRangeAxis.GetPlotAreaCoordinate(tempNum).Value;
                    double num10 = base.ActualDependentRangeAxis.GetPlotAreaCoordinate(base.ActualDependentRangeAxis.Origin).Value;
                    double num11 = (index * Math.Round(a)) + (num5 * 0.1);
                    double num12 = num + num11;
                    if (base.GetIsDataPointGrouped(category))
                    {
                        IGrouping<object, DataPoint> dataPointGroup = base.GetDataPointGroup(category);
                        int num13 = dataPointGroup.IndexOf(dataPoint);
                        num12 += (num13 * (a * 0.2)) / ((double)(dataPointGroup.Count<DataPoint>() - 1));
                        a *= 0.8;
                        Panel.SetZIndex(dataPoint, -num13);
                    }
                    if ((ValueHelper.CanGraph(num9) && ValueHelper.CanGraph(num12)) && ValueHelper.CanGraph(num10))
                    {
                        dataPoint.Visibility = System.Windows.Visibility.Visible;
                        double length = Math.Round(num12);
                        double num15 = Math.Round(a);
                        double num16 = Math.Round((double)((num3 - Math.Max(num9, num10)) + 0.5));
                        double num18 = (Math.Round((double)((num3 - Math.Min(num9, num10)) + 0.5)) - num16) + 1.0;
                        //Canvas.SetLeft(dataPoint, length);
                        Canvas.SetLeft(dataPoint, (num + num2) / 2 - ColumnWidth / 2);
                        Canvas.SetTop(dataPoint, num16);
                        //dataPoint.Width = num15;
                        dataPoint.Width = ColumnWidth;
                        dataPoint.Height = num18 - GetMinNumberPosition(dataPoint);
                    }
                    else
                    {
                        dataPoint.Visibility = System.Windows.Visibility.Collapsed;
                    }
                }
            }
        }

        /// <summary>
        /// 获取最小值的Top坐标
        /// </summary>
        /// <param name="dataPoint"></param>
        /// <returns></returns>
        private double GetMinNumberPosition(DataPoint dataPoint)
        {
            if (base.SeriesHost != null)
            //if ((base.SeriesHost != null) && (base.PlotArea != null))
            {
                object category = dataPoint.ActualIndependentValue ?? (this.ActiveDataPoints.IndexOf(dataPoint) + 1);
                Range<UnitValue> categoryRange = base.GetCategoryRange(category);
                if (categoryRange.HasData)
                {
                    if ((categoryRange.Maximum.Unit != Unit.Pixels) || (categoryRange.Minimum.Unit != Unit.Pixels))
                    {
                        throw new InvalidOperationException("---");
                    }
                    double num = categoryRange.Minimum.Value;
                    double num2 = categoryRange.Maximum.Value;
                    double num3 = base.ActualDependentRangeAxis.GetPlotAreaCoordinate(base.ActualDependentRangeAxis.Range.Maximum).Value;
                    IEnumerable<VerticalColumnSerires> source = from series in base.SeriesHost.Series.OfType<VerticalColumnSerires>()
                                                          where series.ActualIndependentAxis == base.ActualIndependentAxis
                                                          select series;
                    int num4 = source.Count<VerticalColumnSerires>();
                    double num5 = num2 - num;
                    double num6 = num5 * 0.8;
                    double a = num6 / ((double)num4);
                    int index = source.IndexOf(this);
                    //double num9 = base.ActualDependentRangeAxis.GetPlotAreaCoordinate(ValueHelper.ToDouble(dataPoint.ActualDependentValue)).Value;

                    double tempNum = ValueHelper.ToDouble((dataPoint.ActualDependentValue as IVerticalModel).Min);

                    double num9 = base.ActualDependentRangeAxis.GetPlotAreaCoordinate(tempNum).Value;
                    double num10 = base.ActualDependentRangeAxis.GetPlotAreaCoordinate(base.ActualDependentRangeAxis.Origin).Value;
                    double num11 = (index * Math.Round(a)) + (num5 * 0.1);
                    double num12 = num + num11;
                    if (base.GetIsDataPointGrouped(category))
                    {
                        IGrouping<object, DataPoint> dataPointGroup = base.GetDataPointGroup(category);
                        int num13 = dataPointGroup.IndexOf(dataPoint);
                        num12 += (num13 * (a * 0.2)) / ((double)(dataPointGroup.Count<DataPoint>() - 1));
                        a *= 0.8;
                        Panel.SetZIndex(dataPoint, -num13);
                    }
                    if ((ValueHelper.CanGraph(num9) && ValueHelper.CanGraph(num12)) && ValueHelper.CanGraph(num10))
                    {
                        dataPoint.Visibility = System.Windows.Visibility.Visible;
                        double length = Math.Round(num12);
                        double num15 = Math.Round(a);
                        double num16 = Math.Round((double)((num3 - Math.Max(num9, num10)) + 0.5));
                        double num18 = (Math.Round((double)((num3 - Math.Min(num9, num10)) + 0.5)) - num16) + 1.0;
                        ////Canvas.SetLeft(dataPoint, length);
                        //Canvas.SetLeft(dataPoint, (num + num2) / 2 - 5);
                        //Canvas.SetTop(dataPoint, num16);
                        ////dataPoint.Width = num15;
                        //dataPoint.Width = 10;
                        //dataPoint.Height = num18;
                        if (tempNum > 0)
                        {
                            return num18;
                        }
                        else
                        {
                            return 0 - num18;
                        }
                    }
                    else
                    {
                        //dataPoint.Visibility = System.Windows.Visibility.Collapsed;
                    }
                }
            }

            return 0;
        }
    }
}
