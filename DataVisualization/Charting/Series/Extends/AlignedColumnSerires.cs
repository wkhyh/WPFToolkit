﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System.Windows.Controls.DataVisualization.Charting
{
    /// <summary>
    /// 对齐的柱状图
    /// </summary>
    public class AlignedColumnSerires : ColumnBarBaseSeries<ColumnDataPoint>
    {
        /// <summary>
        /// 柱型宽度
        /// </summary>
        public double ColumnWidth { get; set; }

        /// <summary>
        /// AlignedColumnSerires
        /// </summary>
        public AlignedColumnSerires()
        {
            ColumnWidth = 10;
        }

        /// <summary>
        /// GetAxes
        /// </summary>
        /// <param name="firstDataPoint"></param>
        protected override void GetAxes(DataPoint firstDataPoint)
        {
            this.GetAxes(firstDataPoint, axis => axis.Orientation == AxisOrientation.X, () => new CategoryAxis { Orientation = AxisOrientation.X }, delegate(IAxis axis)
            {
                IRangeAxis axis2 = axis as IRangeAxis;
                return ((axis2 != null) && (axis2.Origin != null)) && (axis.Orientation == AxisOrientation.Y);
            }, delegate
            {
                IRangeAxis axis = DataPointSeriesWithAxes.CreateRangeAxisFromData(firstDataPoint.DependentValue);
                axis.Orientation = AxisOrientation.Y;
                if ((axis == null) || (axis.Origin == null))
                {
                    throw new InvalidOperationException("---");
                }
                DisplayAxis axis2 = axis as DisplayAxis;
                if (axis2 != null)
                {
                    axis2.ShowGridLines = true;
                }
                return axis;
            });
        }

        /// <summary>
        /// UpdateDataPoint
        /// </summary>
        /// <param name="dataPoint"></param>
        protected override void UpdateDataPoint(DataPoint dataPoint)
        {
            if (base.SeriesHost != null)
            //if ((base.SeriesHost != null) && (base.PlotArea != null))
            {
                object category = dataPoint.ActualIndependentValue ?? (this.ActiveDataPoints.IndexOf(dataPoint) + 1);
                Range<UnitValue> categoryRange = base.GetCategoryRange(category);
                if (categoryRange.HasData)
                {
                    if ((categoryRange.Maximum.Unit != Unit.Pixels) || (categoryRange.Minimum.Unit != Unit.Pixels))
                    {
                        throw new InvalidOperationException("---");
                    }
                    double num = categoryRange.Minimum.Value;
                    double num2 = categoryRange.Maximum.Value;
                    double num3 = base.ActualDependentRangeAxis.GetPlotAreaCoordinate(base.ActualDependentRangeAxis.Range.Maximum).Value;
                    IEnumerable<AlignedColumnSerires> source = from series in base.SeriesHost.Series.OfType<AlignedColumnSerires>()
                                                             where series.ActualIndependentAxis == base.ActualIndependentAxis
                                                             select series;
                    int num4 = source.Count<AlignedColumnSerires>();
                    double num5 = num2 - num;
                    double num6 = num5 * 0.8;
                    double a = num6 / ((double)num4);
                    int index = source.IndexOf(this);
                    double num9 = base.ActualDependentRangeAxis.GetPlotAreaCoordinate(ValueHelper.ToDouble(dataPoint.ActualDependentValue)).Value;
                    double num10 = base.ActualDependentRangeAxis.GetPlotAreaCoordinate(base.ActualDependentRangeAxis.Origin).Value;
                    double num11 = (index * Math.Round(a)) + (num5 * 0.1);
                    double num12 = num + num11;
                    if (base.GetIsDataPointGrouped(category))
                    {
                        IGrouping<object, DataPoint> dataPointGroup = base.GetDataPointGroup(category);
                        int num13 = dataPointGroup.IndexOf(dataPoint);
                        num12 += (num13 * (a * 0.2)) / ((double)(dataPointGroup.Count<DataPoint>() - 1));
                        a *= 0.8;
                        Panel.SetZIndex(dataPoint, -num13);
                    }
                    if ((ValueHelper.CanGraph(num9) && ValueHelper.CanGraph(num12)) && ValueHelper.CanGraph(num10))
                    {
                        dataPoint.Visibility = Visibility.Visible;
                        double length = Math.Round(num12);
                        double num15 = Math.Round(a);
                        double num16 = Math.Round((double)((num3 - Math.Max(num9, num10)) + 0.5));
                        double num18 = (Math.Round((double)((num3 - Math.Min(num9, num10)) + 0.5)) - num16) + 1.0;
                        //Canvas.SetLeft(dataPoint, length);
                        Canvas.SetLeft(dataPoint, (num + num2) / 2 - ColumnWidth / 2);
                        Canvas.SetTop(dataPoint, num16);
                        //dataPoint.Width = num15;
                        dataPoint.Width = ColumnWidth;
                        dataPoint.Height = num18;
                    }
                    else
                    {
                        dataPoint.Visibility = Visibility.Collapsed;
                    }
                }
            }
        }
    }
}
