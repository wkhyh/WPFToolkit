﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;

namespace System.Windows.Controls.DataVisualization.Charting
{
    public class GeneratedLinearAxis : LinearAxis
    {
        public Brush LabelColorBrush { get; set; }

        protected override Range<IComparable> OverrideDataRange(Range<IComparable> range)
        {
            var result = base.OverrideDataRange(range);

            if (range.HasData)
            {
                var currentRange = range.ToDoubleRange();

                var minimum = (int)currentRange.Minimum;
                var maximum = (int)currentRange.Maximum + 1;
                minimum = minimum - (minimum % 10);
                maximum = maximum + 10 - (maximum % 10);

                currentRange = new Range<double>(minimum, maximum);
                Interval = (maximum - minimum) / 5;

                this.Maximum = maximum;
                this.Minimum = minimum;

                result = currentRange.ToComparableRange();
            }

            return result;
        }

        protected override IEnumerable<IComparable> GetMajorTickMarkValues(Size availableSize)
        {
            return GetLabelValues(availableSize);
        }

        protected override IEnumerable<IComparable> GetMinorTickMarkValues(Size availableSize)
        {
            return GetLabelValues(availableSize);
        }

        protected override IEnumerable<IComparable> GetLabelValues(Size availableSize)
        {
            //return base.GetLabelValues(availableSize);

            var list = new List<IComparable>();

            var num = this.Minimum;
            while(num <= this.Maximum)
            {
                list.Add(num);
                num += this.Interval;
            }

            return list;
        }

        protected override Control CreateAxisLabel()
        {
            var label = new AxisLabel();
            if (LabelColorBrush != null)
            {
                //label.Foreground = new SolidColorBrush(Colors.Yellow);
                label.Foreground = LabelColorBrush;
            }

            return label;
        }
    }
}
